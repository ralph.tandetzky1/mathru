mod gauss_legendre_impl;
mod root_weight;

pub use gauss_legendre_impl::GaussLegendre;
pub use root_weight::RootWeight;
