mod closedfixedintervaliterator;
mod newton_cotes_impl;

pub use closedfixedintervaliterator::ClosedFixedIntervalIterator;
pub use newton_cotes_impl::NewtonCotes;
